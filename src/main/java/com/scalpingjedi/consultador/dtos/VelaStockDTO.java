package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import com.scalpingjedi.comun.entidades.pools.Pool;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VelaStockDTO {

	private Long date;
	private BigDecimal open;
	private BigDecimal close;
	private BigDecimal high;
	private BigDecimal low;
	private Long volume;
	private BigDecimal value;
	
	public VelaStockDTO(LocalDateTime fecha, BigDecimal open,BigDecimal close, BigDecimal high, BigDecimal low,  BigDecimal volume , BigDecimal value) {
//		SimpleDateFormat dd = new SimpleDateFormat("dd/mm/yyyy HH:mm");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		this.date = Date.from(fecha.atZone(ZoneId.systemDefault()).toInstant()).getTime();
		this.open = open;
		this.close = close;
		this.high = high;
		this.low = low;
		this.volume = volume.longValue();
		this.value = value;
	}
	
	public String getFechaSeconds(LocalDateTime fecha) {
		return "" + fecha.toEpochSecond(ZoneOffset.UTC);
	}
}
