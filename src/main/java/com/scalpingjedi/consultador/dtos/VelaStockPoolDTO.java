package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.scalpingjedi.comun.entidades.helpers.FechaHelperComun;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class VelaStockPoolDTO {

	private Long date;
	private BigDecimal open;
	private BigDecimal close;
	private BigDecimal high;
	private BigDecimal low;
	private Long volume;
	private BigDecimal precio;
	private BigDecimal suma100;
	private BigDecimal suma50;
	private BigDecimal suma25;
	private BigDecimal sumaTotal;
	private String tooltip;
	private final String tooltipTemplate = "{0}:{1}:{2}:{3}:{4}";
	
	
	
	public VelaStockPoolDTO(LocalDateTime fecha, BigDecimal open,BigDecimal close, BigDecimal high, BigDecimal low,  BigDecimal volume , Integer precio,BigDecimal suma100,BigDecimal suma50,BigDecimal suma25,BigDecimal sumaTotal) {
		this.date = Date.from(fecha.atZone(ZoneOffset.UTC).toInstant()).getTime();
		this.open = open;
		this.close = close;
		this.high = high;
		this.low = low;
		if (precio == null) {
			this.precio = BigDecimal.ZERO;
		}else {
			this.precio = new BigDecimal(precio);
		}
		if (suma25 == null) {
			this.suma25 = BigDecimal.ZERO;
		}else {
			this.suma25 = suma25.divide(new BigDecimal(1000000), 1, RoundingMode.HALF_UP);
		}
		if (suma50 == null) {
			this.suma50 = BigDecimal.ZERO;
		}else {
			this.suma50 = suma50.divide(new BigDecimal(1000000), 1, RoundingMode.HALF_UP);
		}
		if (suma100 == null) {
			this.suma100 = BigDecimal.ZERO;
		}else {
			this.suma100 = suma100.divide(new BigDecimal(1000000), 1, RoundingMode.HALF_UP);
		}
		this.sumaTotal = this.suma100.add(this.suma50).add(this.suma25);
		
		if (sumaTotal == null) {
			this.sumaTotal = BigDecimal.ZERO;
			this.volume = 0L;
		}else {
			this.volume = sumaTotal.longValue();
		}
		this.tooltip = MessageFormat.format(tooltipTemplate, this.precio.setScale(0).toPlainString(), this.suma25.setScale(1).toPlainString(), this.suma50.setScale(1).toPlainString(), this.suma100.setScale(1).toPlainString(), this.sumaTotal.setScale(1).toPlainString());
	}
	
	public String getFechaSeconds(LocalDateTime fecha) {
		return "" + fecha.toEpochSecond(ZoneOffset.UTC);
	}
	
	public String getHoraCierreString(String patron) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patron);
		LocalDateTime fecha = FechaHelperComun.dateToLocalDateTime(new Date(this.getDate()));
		String formatDateTime = fecha.format(formatter);
		return formatDateTime;
	}

}
