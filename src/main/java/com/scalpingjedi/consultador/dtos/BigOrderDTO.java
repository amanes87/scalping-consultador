package com.scalpingjedi.consultador.dtos;

import java.time.ZoneOffset;
import java.util.Date;

import com.scalpingjedi.comun.entidades.trades.BigOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BigOrderDTO {
	
	private String side;
	private Long f;
	private String s;	
	private String p;
	private String precioLiquidacion25Long;
	private String precioLiquidacion50Long;
	private String precioLiquidacion100Long;
	private String precioLiquidacion25Short;
	private String precioLiquidacion50Short;
	private String precioLiquidacion100Short;
	
	public BigOrderDTO(BigOrder bigOrder) {
		this.s = bigOrder.getVolumen().setScale(0).toString();
		this.f = Date.from(bigOrder.getHora().atZone(ZoneOffset.UTC).toInstant()).getTime();
		this.p = bigOrder.getPrice().setScale(1).toString();
	}
	
}
