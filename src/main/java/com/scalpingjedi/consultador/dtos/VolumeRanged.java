package com.scalpingjedi.consultador.dtos;

import com.scalpingjedi.comun.entidades.trades.ResumenBigOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VolumeRanged {

	private String fecha;
	private Integer cant1000;
	private Integer cant5000;
	private Integer cant20000;
	private Integer cant100000;
	private Integer cant250000;
	private Integer cant500000;
	private Integer cantINF;
	
	public VolumeRanged(ResumenBigOrder resumenBigOrder) {
		this.fecha = resumenBigOrder.getFechaSeconds();
		this.cant1000 = resumenBigOrder.getCant1000();
		this.cant5000 = resumenBigOrder.getCant5000();
		this.cant20000 = resumenBigOrder.getCant20000();
		this.cant100000 = resumenBigOrder.getCant100000();
		this.cant250000 = resumenBigOrder.getCant250000();
		this.cant500000 = resumenBigOrder.getCant500000();
		this.cantINF = resumenBigOrder.getCantINF();
				
	}
}
