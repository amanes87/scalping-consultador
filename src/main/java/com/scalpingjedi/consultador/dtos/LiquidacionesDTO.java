package com.scalpingjedi.consultador.dtos;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LiquidacionesDTO {

	
	private String size;
	private Long fecha;
	private String side;
	private String price;
	
	public LiquidacionesDTO(String size, Long fecha, String side, String price) {
		this.side = side;
		this.size = size;
		this.fecha = fecha;
		this.price = price;
	}
	
}
