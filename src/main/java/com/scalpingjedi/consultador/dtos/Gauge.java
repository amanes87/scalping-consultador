package com.scalpingjedi.consultador.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Gauge {

	private String name;
	
	private Integer value;
	
	public Gauge(String name, int value) {
		this.name = name;
		this.value = value;
	}
	
}
