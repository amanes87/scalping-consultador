package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.scalpingjedi.comun.entidades.VelaBitmex;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class MainGraph {

	// DATOS VELAS (tiempo, high, low, close, open, volumen)
	private String t;
	private String h;
	private String l;
	private String o;
	private String c;
	private Integer v;

	// DATOS POOLS
	private BigDecimal precio;
	private BigDecimal suma100;
	private BigDecimal suma50;
	private BigDecimal suma25;
	private BigDecimal sumaTotal;

	public MainGraph(VelaBitmex vela) {
		this.t = vela.getHoraCierreString("HH:mm");
		this.h = vela.getHigh().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.l = vela.getLow().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.o = vela.getOpen().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.c = vela.getClose().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.v = vela.getVolumen().intValue();
	}

	public MainGraph(VelaStockPoolDTO vela, String a) {
		this.t = vela.getHoraCierreString("HH:mm");
		this.h = null;
		this.l = null;
		this.o = null;
		this.c = null;
		if(vela.getSumaTotal() != null) {
			this.v = vela.getSumaTotal().intValue();
		}else {
			this.v = 0;
		}
		this.precio = vela.getPrecio();
		this.suma100 = vela.getSuma100();
		this.suma50 = vela.getSuma50();
		this.suma25 = vela.getSuma25();
		this.sumaTotal = vela.getSumaTotal();
	}

	
	public MainGraph(VelaStockPoolDTO vela) {
		this.t = vela.getHoraCierreString("HH:mm");
		this.h = vela.getHigh().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.l = vela.getLow().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.o = vela.getOpen().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.c = vela.getClose().setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.v = vela.getVolume().intValue();
		this.precio = vela.getPrecio();
		this.suma100 = vela.getSuma100();
		this.suma50 = vela.getSuma50();
		this.suma25 = vela.getSuma25();
		this.sumaTotal = vela.getSumaTotal();
	}

}
