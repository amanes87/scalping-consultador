package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PoolsInfoDTO{
	
	private Double minPoolPrice;
	private Double maxPoolPrice;
	private List<TooltipDTO> tooltips;
	
	public PoolsInfoDTO(BigDecimal minPrecio, BigDecimal maxPrecio, Map<String, String> mapTooltips) {
		this.minPoolPrice = minPrecio.setScale(0).doubleValue();
		this.maxPoolPrice = maxPrecio.setScale(0).doubleValue();
		this.tooltips = mapTooltips.entrySet().stream().map(es -> new TooltipDTO(es.getKey(), es.getValue())).collect(Collectors.toList());
	}
	
}