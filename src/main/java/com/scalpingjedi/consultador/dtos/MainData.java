package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Date;

import org.threeten.bp.OffsetDateTime;

import com.scalpingjedi.comun.entidades.helpers.FechaHelperComun;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MainData {

	private Long fecha;
	private String buy_count;
	private String buy_price;
	private String buy_size;
	private String sell_count;
	private String sell_price;
	private String sell_size;

	// DATOS LONG
	private String long100x_kill;
	private String long100x_entrada;
	private String long100x_profit;
	private String long50x_kill;
	private String long50x_entrada;
	private String long50x_profit;
	private String long25x_kill;
	private String long25x_entrada;
	private String long25x_profit;

	// DATOS SHORT
	private String short100x_kill;
	private String short100x_entrada;
	private String short100x_profit;
	private String short50x_kill;
	private String short50x_entrada;
	private String short50x_profit;
	private String short25x_kill;
	private String short25x_entrada;
	private String short25x_profit;

	// DATOS FUNDING
	private String fundingTime;
	private BigDecimal fundingRate;
	private BigDecimal fundingRateDaily;
		
	public MainData(BigDecimal buyCount, BigDecimal buyPrice, BigDecimal buySize, BigDecimal sellCount,
			BigDecimal sellPrice, BigDecimal sellSize, LocalDateTime fechaUltimoKill, OffsetDateTime fundingTime, Double fundingRate, Double fundingRateDaily) {
		this.fecha = FechaHelperComun.getTimeDeLocalDateTime(fechaUltimoKill);
		this.buy_count = buyCount.setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.buy_price = buyPrice.setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.buy_size = buySize.setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.sell_count = sellCount.setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.sell_price = sellPrice.setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.sell_size = sellSize.setScale(1, RoundingMode.HALF_UP).toPlainString();
		if (null != sellPrice) {
			setearShortValues(sellPrice.doubleValue());
		}
		if (null != buyPrice) {
			setearLongValues(buyPrice.doubleValue());
		}
		if (fundingRate != null) {
			this.fundingRate = new BigDecimal(fundingRate*100);
		}
		if (fundingRateDaily != null) {
			this.fundingRateDaily = new BigDecimal(fundingRateDaily*100);
		}
		if (fundingTime != null) {
			this.fundingTime =  FechaHelperComun.dateToLocalDateTime(new Date(fundingTime.toEpochSecond() * 1000)).atOffset(java.time.ZoneOffset.UTC).toString();
		}
	}
	

	public void setearShortValues(Double sellPrice) {
		this.short100x_kill = new BigDecimal((sellPrice * 1.0047)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short100x_entrada = new BigDecimal((sellPrice * 1.0045)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short100x_profit = new BigDecimal((sellPrice * 1.001)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short50x_kill = new BigDecimal((sellPrice * 1.0147)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short50x_entrada = new BigDecimal((sellPrice * 1.014)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short50x_profit = new BigDecimal((sellPrice * 1.0085)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short25x_kill = new BigDecimal((sellPrice * 1.0339)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short25x_entrada = new BigDecimal((sellPrice * 1.032)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.short25x_profit = new BigDecimal((sellPrice * 1.021)).setScale(1, RoundingMode.HALF_UP).toPlainString();
	}

	public void setearLongValues(Double buyPrice) {
		this.long100x_kill = new BigDecimal((buyPrice * 0.9953)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long100x_entrada = new BigDecimal((buyPrice * 0.9955)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long100x_profit = new BigDecimal((buyPrice * 0.999)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long50x_kill = new BigDecimal((buyPrice * 0.9853)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long50x_entrada = new BigDecimal((buyPrice * 0.986)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long50x_profit = new BigDecimal((buyPrice * 0.9915)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long25x_kill = new BigDecimal((buyPrice * 0.968)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long25x_entrada = new BigDecimal((buyPrice * 0.9661)).setScale(1, RoundingMode.HALF_UP).toPlainString();
		this.long25x_profit = new BigDecimal((buyPrice * 0.979)).setScale(1, RoundingMode.HALF_UP).toPlainString();
	}

}
