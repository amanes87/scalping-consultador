package com.scalpingjedi.consultador.dtos;

import lombok.Getter;

public enum TipoGauge {

	LAST_KILL("lastkill"), MIN30("30min"), HOUR1("1hour"), HOUR2("2hour"), HOUR4("4hour");
	
	@Getter
	String codigo;
	
	private TipoGauge(String codigo) {
		this.codigo = codigo;
	}
	
}
