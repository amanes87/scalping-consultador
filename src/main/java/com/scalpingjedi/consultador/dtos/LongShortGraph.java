package com.scalpingjedi.consultador.dtos;

import java.math.RoundingMode;

import com.scalpingjedi.comun.entidades.trades.ResumenTrade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LongShortGraph {

	private String time;
	
	//Longs
	private Integer buy_count;
	private Integer buy_size;
	private String buy_price;
	
	//Shorts
	private Integer sell_count;
	private Integer sell_size;
	private String sell_price;
	
	public LongShortGraph(ResumenTrade resumenTrade) {
		this.time = resumenTrade.getFechaCierreString("HH:mm");
		
		this.buy_count = resumenTrade.getCountLong();
		this.buy_size = resumenTrade.getVolumenLong().intValue();
		this.buy_price = resumenTrade.getPrecioLong().setScale(1, RoundingMode.HALF_UP).toPlainString();;
		
		this.sell_count = resumenTrade.getCountShort();
		this.sell_size = resumenTrade.getVolumenShort().intValue();
		this.sell_price = resumenTrade.getPrecioShort().setScale(1, RoundingMode.HALF_UP).toPlainString();;
	}
}
