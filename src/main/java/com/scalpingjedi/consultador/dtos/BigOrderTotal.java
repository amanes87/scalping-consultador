package com.scalpingjedi.consultador.dtos;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import com.scalpingjedi.comun.entidades.trades.ResumenTrade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BigOrderTotal {

	// Datos Short
	private String fs; //fecha
	private String ps; //precio
	private String vs; // volumen int
	private String ts; //tamaño
	private String cs; //color

	// Datos Long
	private String fl;
	private String pl;
	private String vl; // int
	private String tl;
	private String color2;

	// Datos vela
	private String o;
	private String h;
	private String l;
	private String c;

	private String pl2l; //Precio Liquidacion 25 Long
	private String pl5l;//Precio Liquidacion 50 Long
	private String pl1l; //Precio Liquidacion 25 Long

	private String pl2s; //Precio Liquidacion 25 Short
	private String pl5s; //Precio Liquidacion 50 Short
	private String pl1s;//Precio Liquidacion 100 Short
	

	public BigOrderTotal(ResumenTrade resumenTrade) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = df.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		df.setDecimalFormatSymbols(symbols);
		
		
		this.fs = resumenTrade.getFechaCierreString("HH:mm");
		this.ps = resumenTrade.getPrecioShort().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		this.vs = df.format(resumenTrade.getVolumenMayorShort().setScale(0, BigDecimal.ROUND_HALF_UP).setScale(0, BigDecimal.ROUND_HALF_UP));
		this.ts = df.format(resumenTrade.getTamanoShort().setScale(1, BigDecimal.ROUND_HALF_UP));

		this.fl = fs;
		this.pl = resumenTrade.getPrecioLong().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		this.vl = df.format(resumenTrade.getVolumenMayorLong().setScale(0, BigDecimal.ROUND_HALF_UP).setScale(0, BigDecimal.ROUND_HALF_UP));
		this.tl = df.format(resumenTrade.getTamanoLong().setScale(1, BigDecimal.ROUND_HALF_UP));

		this.o = resumenTrade.getOpen().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		this.h = resumenTrade.getHigh().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		this.l = resumenTrade.getLow().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		this.c = resumenTrade.getClose().setScale(1, BigDecimal.ROUND_HALF_UP).toPlainString();
		
		this.pl1l = df.format(resumenTrade.getPrecioLiquidacion100Long().setScale(0, BigDecimal.ROUND_HALF_UP));
		this.pl5l= df.format(resumenTrade.getPrecioLiquidacion50Long().setScale(0, BigDecimal.ROUND_HALF_UP));
		this.pl2l= df.format(resumenTrade.getPrecioLiquidacion25Long().setScale(0, BigDecimal.ROUND_HALF_UP));

		this.pl1s= df.format(resumenTrade.getPrecioLiquidacion100Short().setScale(0, BigDecimal.ROUND_HALF_UP));
		this.pl5s= df.format(resumenTrade.getPrecioLiquidacion50Short().setScale(0, BigDecimal.ROUND_HALF_UP));
		this.pl2s= df.format(resumenTrade.getPrecioLiquidacion25Short().setScale(0, BigDecimal.ROUND_HALF_UP));
		
		
	}

}
