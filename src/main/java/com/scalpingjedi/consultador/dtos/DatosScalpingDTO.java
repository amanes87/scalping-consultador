package com.scalpingjedi.consultador.dtos;

import java.util.ArrayList;
import java.util.List;

import com.scalpingjedi.comun.entidades.kills.Kill;
import com.scalpingjedi.comun.entidades.trades.BigOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DatosScalpingDTO {


	//DATOS FIJOS
	private List<BigOrderTotal> bigorder_total;
	private List<Kill> list_kills;
	private List<Gauge> gauges;
	private List<LongShortGraph> longshortgraph;
	
	private List<MainGraph> maingraph;
	private List<VolumeRanged> volumeranged;
	private List<BigOrderDTO> bigorder_long;
	private List<BigOrderDTO> bigorder_short;
	private List<LiquidacionesDTO> liquidaciones;
	
	private MainData maindata;

	public void setBigOrders(List<BigOrder> datosBigOrders) {
		this.bigorder_short = new ArrayList<>();
		this.bigorder_long = new ArrayList<>();
		datosBigOrders.forEach(dbo -> {
			if ("Sell".equals(dbo.getSide())) {
				this.bigorder_short.add(new BigOrderDTO(dbo));
			}else {
				this.bigorder_long.add(new BigOrderDTO(dbo));
			}
		});
	}
	
}
