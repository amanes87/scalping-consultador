package com.scalpingjedi.consultador.dtos.pools.tabla;

import java.math.RoundingMode;

import com.scalpingjedi.consultador.dtos.VelaStockPoolDTO;

public class PoolsDTO {

	public Double p;
	public Double x25;
	public Double x50;
	public Double x100;
	public Double t;

	public PoolsDTO(VelaStockPoolDTO valorPool) {
		this.p = valorPool.getPrecio().setScale(0, RoundingMode.HALF_DOWN).doubleValue();
		this.x25 = valorPool.getSuma25().setScale(1, RoundingMode.HALF_UP).doubleValue();
		this.x50 = valorPool.getSuma50().setScale(1, RoundingMode.HALF_UP).doubleValue();
		this.x100 = valorPool.getSuma100().setScale(1, RoundingMode.HALF_UP).doubleValue();
		this.t = valorPool.getSumaTotal().setScale(1, RoundingMode.HALF_UP).doubleValue();
	}
}
