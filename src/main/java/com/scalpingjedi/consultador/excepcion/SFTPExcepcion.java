package com.scalpingjedi.consultador.excepcion;

import org.springframework.context.annotation.PropertySource;

import com.scalpingjedi.comun.entidades.excepcion.ExcepcionTelegram;
import com.telegram.service.TelegramSv;

@PropertySource("classpath:aplicacion.properties")
public class SFTPExcepcion extends ExcepcionTelegram{

	private static final long serialVersionUID = -5676467450042156388L;

	public SFTPExcepcion(Throwable e, String mensaje, TelegramSv telegramSv) {
		super(e, mensaje, telegramSv);
	}

}
