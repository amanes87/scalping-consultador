package com.scalpingjedi.consultador.excepcion;

import org.springframework.context.annotation.PropertySource;

import com.scalpingjedi.comun.entidades.excepcion.ExcepcionTelegram;
import com.telegram.service.TelegramSv;

@PropertySource("classpath:aplicacion.properties")
public class SshConnectorExcepcion extends ExcepcionTelegram {

	private static final long serialVersionUID = -5638056847026142878L;
	
	public SshConnectorExcepcion(Exception e, String mensaje, TelegramSv telegramSv) {
		super(e, mensaje, telegramSv);
	}

}
