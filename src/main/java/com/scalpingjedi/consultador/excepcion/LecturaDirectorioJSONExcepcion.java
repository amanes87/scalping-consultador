package com.scalpingjedi.consultador.excepcion;

import org.springframework.context.annotation.PropertySource;

import com.scalpingjedi.comun.entidades.excepcion.ExcepcionTelegram;
import com.telegram.service.TelegramSv;

@PropertySource("classpath:aplicacion.properties")
public class LecturaDirectorioJSONExcepcion extends ExcepcionTelegram {

	private static final long serialVersionUID = 4641014723335212684L;

	public LecturaDirectorioJSONExcepcion(Throwable e, String mensaje, TelegramSv telegramSv) {
		super(e, mensaje, telegramSv);
	}
}
