package com.scalpingjedi.consultador.excepcion;

import com.scalpingjedi.comun.entidades.excepcion.ExcepcionTelegram;
import com.telegram.service.TelegramSv;

public class ConstruirArchivosExcepcion extends ExcepcionTelegram{

	private static final long serialVersionUID = -1084945685664468016L;

	public ConstruirArchivosExcepcion(Throwable e, String mensaje, TelegramSv telegramSv) {
		super(e, mensaje, telegramSv);
	}
}
