package com.scalpingjedi.consultador.excepcion;

import org.springframework.context.annotation.PropertySource;

import com.scalpingjedi.comun.entidades.excepcion.ExcepcionTelegram;
import com.telegram.service.TelegramSv;
@PropertySource("classpath:aplicacion.properties")
public class HeatmapExcepcion extends ExcepcionTelegram{

	private static final long serialVersionUID = 164500344101882934L;

	public HeatmapExcepcion(Throwable e, String mensaje, TelegramSv telegramSv) {
		super(e, mensaje, telegramSv);
	}
}
