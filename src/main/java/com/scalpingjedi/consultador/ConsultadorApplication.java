package com.scalpingjedi.consultador;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.scalpingjedi.consultador.servicio.DatosEnMemoria;

@SpringBootApplication
@ComponentScan({ "com.scalpingjedi" })
@EntityScan("com.scalpingjedi")
@EnableJpaRepositories("com.scalpingjedi")
@PropertySource("classpath:aplicacion.properties")
public class ConsultadorApplication implements CommandLineRunner {
	
	@Autowired
	private HiloSFTP hiloSFTP;
	
	@Autowired
	private DatosEnMemoria datosEnMemoria;
	
	@Value("${telegram.chatId}")
	private String chatId;
	
	@Value("${telegram.url}")
	private String url;
	
	@Value("${spring.profiles.active}")
	private String entorno;

	public static void main(String[] args) {
		SpringApplication.run(ConsultadorApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		datosEnMemoria.inicializarTelegram(chatId, url);
		datosEnMemoria.notificarMensaje(MessageFormat.format("ARRANQUE CONSULTADOR ({0})", entorno));
		if (datosEnMemoria.isServidor()) {
			hiloSFTP.ejecutarDatosGenerales(args);
			hiloSFTP.ejecutarPools(args);
		}
	}

}
