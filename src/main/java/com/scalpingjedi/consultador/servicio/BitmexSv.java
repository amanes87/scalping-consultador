package com.scalpingjedi.consultador.servicio;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.threeten.bp.OffsetDateTime;

import com.scalpingjedi.comun.entidades.VelaBitmex;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;

import io.swagger.client.ApiException;
import io.swagger.client.api.FundingApi;
import io.swagger.client.api.TradeApi;
import io.swagger.client.model.Funding;
import io.swagger.client.model.TradeBin;

/**
 * Servicio encargado de llamar a los WebServices de Bitmex
 * 
 * @author Adrià
 *
 */
@Component
public class BitmexSv {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public Funding obtenerFunding() {
		FundingApi apiInstance = new FundingApi();
		OffsetDateTime fechaInicio = OffsetDateTime.now().minusDays(1L);
		OffsetDateTime fechaFin = OffsetDateTime.now().plusDays(1L);
		try {
			List<Funding> fundings = apiInstance.fundingGet("XBTUSD", "", "", BigDecimal.ONE, BigDecimal.ZERO, true, fechaInicio, fechaFin);
			if (fundings != null && !fundings.isEmpty()) {
				return fundings.get(0);
			}
			return null;
		} catch (ApiException e) {
			log.error("StackTrace del error: ", e);
		}
		return null;
	}

}
