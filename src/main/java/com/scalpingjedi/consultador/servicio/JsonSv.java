package com.scalpingjedi.consultador.servicio;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tc33.jheatchart.HeatChart;
import org.threeten.bp.OffsetDateTime;

import com.google.gson.Gson;
import com.scalpingjedi.comun.entidades.VelaBitmex;
import com.scalpingjedi.comun.entidades.helpers.FechaHelperComun;
import com.scalpingjedi.comun.entidades.kills.Kill;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;
import com.scalpingjedi.comun.entidades.trades.BigOrder;
import com.scalpingjedi.comun.entidades.trades.ResumenBigOrder;
import com.scalpingjedi.comun.entidades.trades.ResumenTrade;
import com.scalpingjedi.comun.servicios.Vela_005Repository;
import com.scalpingjedi.comun.servicios.kills.KillRepository;
import com.scalpingjedi.comun.servicios.pools.PoolRepository;
import com.scalpingjedi.comun.servicios.trades.BigOrderRepository;
import com.scalpingjedi.comun.servicios.trades.ResumenBigOrderRepository;
import com.scalpingjedi.comun.servicios.trades.ResumenTradeRepository;
import com.scalpingjedi.consultador.FiltroMillones;
import com.scalpingjedi.consultador.TemporalidadJSON;
import com.scalpingjedi.consultador.controladores.JsonSvHelper;
import com.scalpingjedi.consultador.dtos.BigOrderTotal;
import com.scalpingjedi.consultador.dtos.DatosScalpingDTO;
import com.scalpingjedi.consultador.dtos.Gauge;
import com.scalpingjedi.consultador.dtos.LiquidacionesDTO;
import com.scalpingjedi.consultador.dtos.LongShortGraph;
import com.scalpingjedi.consultador.dtos.MainData;
import com.scalpingjedi.consultador.dtos.MainGraph;
import com.scalpingjedi.consultador.dtos.TipoGauge;
import com.scalpingjedi.consultador.dtos.VelaStockPoolDTO;
import com.scalpingjedi.consultador.dtos.VolumeRanged;
import com.scalpingjedi.consultador.excepcion.ConstruirPoolsExcepcion;
import com.scalpingjedi.consultador.excepcion.HeatmapExcepcion;
import com.scalpingjedi.consultador.excepcion.JsonExcepcion;

import io.swagger.client.model.Funding;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Component
public class JsonSv {

	@Autowired
	private Vela_005Repository vela_005Repository;

	@Autowired
	private ResumenTradeRepository resumenTradeRepository;

	@Autowired
	private ResumenBigOrderRepository resumenBigOrderRepository;

	@Autowired
	private BigOrderRepository bigOrderRepository;

	@Autowired
	private KillRepository killRepository;

	@Autowired
	private PoolRepository poolRepository;

	@Autowired
	private DatosEnMemoria datosEnMemoria;

	@Autowired
	private BitmexSv bitmexSv;

	@Value("${entorno.heatmap.ruta.full}")
	private String rutaHeatmapFull;

	@Value("${entorno.heatmap.ruta}")
	private String rutaHeatmap;

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public String obtenerJSONTemporalidad(String timeFrame, LocalDateTime horaInicio) {
		DatosScalpingDTO datosScalpingDTO = new DatosScalpingDTO();
		List<ResumenTrade> resumenesTrade = resumenTradeRepository.todosRegistrosAscendiente();
		resumenesTrade = resumenesTrade.stream().filter(rt -> rt.getFechaCierre().compareTo(horaInicio) > 0).sorted((rt1, rt2) -> rt1.getFechaCierre().compareTo(rt2.getFechaCierre())).collect(Collectors.toList());
		datosScalpingDTO.setBigorder_total(obtenerDatosBigOrderTotal(resumenesTrade));
		datosScalpingDTO.setLongshortgraph(obtenerDatosLongShortGraph(resumenesTrade));
		datosScalpingDTO.setList_kills(obtenerDatosListKills(horaInicio));
		LocalDateTime horaBusqueda = LocalDateTime.now(ZoneOffset.UTC);
		TemporalidadJSON temporalidadElegida = TemporalidadJSON.getTemporalidad(timeFrame);
		switch (temporalidadElegida) {
		case MINUTOS_30:
			rellenarDatos(datosScalpingDTO, horaBusqueda.minusMinutes(30), temporalidadElegida);
			break;
		case HORAS_1:
			rellenarDatos(datosScalpingDTO, horaBusqueda.minusHours(1), temporalidadElegida);
			break;
		case HORAS_2:
			rellenarDatos(datosScalpingDTO, horaBusqueda.minusHours(2), temporalidadElegida);
			break;
		case HORAS_4:
			rellenarDatos(datosScalpingDTO, horaBusqueda.minusHours(4), temporalidadElegida);
			break;
		default:
			rellenarDatos(datosScalpingDTO, getHoraUltimoKill(), TemporalidadJSON.LAST_KILL);
			break;
		}
		datosScalpingDTO.setGauges(obtenerDatosGauges());
		Gson gson = new Gson();
		return gson.toJson(datosScalpingDTO);

	}

	private LocalDateTime getHoraUltimoKill() {
		LocalDateTime horaUltimoKill = LocalDateTime.now(ZoneOffset.UTC);
		try {
			List<Kill> ultimaKillList = killRepository.getLastKill();

			if (CollectionUtils.isNotEmpty(ultimaKillList)) {
				horaUltimoKill = ultimaKillList.get(0).getFecha();
			}

		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante getHoraUltimoKill" + e);
		}
		return horaUltimoKill;
	}

	private void rellenarDatos(DatosScalpingDTO datos, LocalDateTime horaAnterior, TemporalidadJSON temporalidadElegida) {
		try {
			datos.setVolumeranged(obtenerDatosVolumeRanged(horaAnterior));
			datos.setBigOrders(obtenerDatosBigOrders(horaAnterior));
			datos.setMaindata(obtenerDatosMainData(horaAnterior, temporalidadElegida));
			datos.setLiquidaciones(obtenerLiquidaciones(horaAnterior));
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage(MessageFormat.format("ExcepciÃ³n durante rellenar datos de temporalidad {0}", temporalidadElegida.getCodigo()) + e);
		}
	}

	public static BigDecimal retornarNumeroAgrupado(BigDecimal precio) {
		BigDecimal precioRango = precio.divide(new BigDecimal(5), 0, RoundingMode.DOWN);
		return precioRango.multiply(new BigDecimal(5));
	}

	private List<BigOrderTotal> obtenerDatosBigOrderTotal(List<ResumenTrade> resumenesTrade) {
		List<BigOrderTotal> listBigOrderTotal = new ArrayList<>();
		try {
			resumenesTrade.forEach(rt -> {
				BigOrderTotal bigOrderTotal = new BigOrderTotal(rt);

				listBigOrderTotal.add(bigOrderTotal);
			});
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("ExcepciÃ³n durante obtenerDatosBigOrderTotal" + e);
		}

		return listBigOrderTotal;
	}

	private List<VolumeRanged> obtenerDatosVolumeRanged(LocalDateTime hora) throws JsonExcepcion {
		List<VolumeRanged> listVolumeranged = new ArrayList<>();
		try {
			List<ResumenBigOrder> resumenBigOrder = resumenBigOrderRepository.findAllAfterDate(hora);
			resumenBigOrder = resumenBigOrder.stream().sorted((rbo1, rbo2) -> rbo1.getFecha().compareTo(rbo2.getFecha())).collect(Collectors.toList());
			resumenBigOrder.forEach(rbo -> {
				VolumeRanged volumeRanged = new VolumeRanged(rbo);
				listVolumeranged.add(volumeRanged);
			});

		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("ExcepciÃ³n durante obtenerDatosVolumeRanged" + e);
			throw e;
		}
		return listVolumeranged;

	}

	private List<BigOrder> obtenerDatosBigOrders(LocalDateTime hora) throws JsonExcepcion {
		List<BigOrder> bigOrders = new ArrayList<>();
		try {
			bigOrders = bigOrderRepository.findAllAfterDate(hora);
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerBigOrders" + e);
		}
		return bigOrders;
	}

	// recordar grabar el precio de las liquidaciones de short y long

	private List<LiquidacionesDTO> obtenerLiquidaciones(LocalDateTime hora) throws JsonExcepcion {
		List<Vela_005> velas = new ArrayList<>();
		List<LiquidacionesDTO> liquidaciones = new ArrayList<>();

		try {
			velas = vela_005Repository.findAllAfterDate(hora).stream().filter(VelaBitmex.fechaIgualOSuperiorA(hora)).sorted(VelaBitmex.ORDEN_ASCENDENTE).collect(Collectors.toList());
			for (Vela_005 vela : velas) {

				if (vela.getLongLiquidation() != null && vela.getLongLiquidation()) {
					liquidaciones.add(new LiquidacionesDTO(vela.getLongLiquidationQuantity().toString(), Date.from(vela.getHoraCierre().atZone(ZoneOffset.UTC).toInstant()).getTime(), "Long", vela.getPrecioMedioBuy().toPlainString()));
				}

				if (vela.getShortLiquidation() != null && vela.getShortLiquidation()) {
					liquidaciones.add(new LiquidacionesDTO(vela.getShortLiquidationQuantity().toString(), Date.from(vela.getHoraCierre().atZone(ZoneOffset.UTC).toInstant()).getTime(), "Short", vela.getPrecioMedioSell().toPlainString()));
				}
			}

			liquidaciones = liquidaciones.stream().sorted((l1, l2) -> l2.getFecha().compareTo(l1.getFecha())).collect(Collectors.toList());
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante liquidaciones" + e);
			throw e;
		}
		return liquidaciones;
	}

	private List<LongShortGraph> obtenerDatosLongShortGraph(List<ResumenTrade> resumenesTrade) {
		List<LongShortGraph> listLongShortGraph = new ArrayList<>();
		try {
			resumenesTrade.forEach(rt -> {
				LongShortGraph longShortGraph = new LongShortGraph(rt);
				listLongShortGraph.add(longShortGraph);
			});
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerDatosLongShortGraph" + e);
		}
		return listLongShortGraph;
	}

	private List<Kill> obtenerDatosListKills(LocalDateTime horaInicio) {
		List<Kill> kills = new ArrayList<>();
		try {
			kills = killRepository.todosRegistrosAscendiente();
			kills.stream().forEach(k -> {
				k.setFechaLong(FechaHelperComun.getTimeDeLocalDateTime(k.getFecha()));
			});
			kills = kills.stream().filter(k -> k.getFecha().isAfter(horaInicio)).sorted((k1, k2) -> k2.getFecha().compareTo(k1.getFecha())).collect(Collectors.toList());
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerDatosListKills" + e);
		}
		return kills;
	}

	// TODO MIRAR EL VALUE QUE PONEMOS
	private List<Gauge> obtenerDatosGauges() {

		List<Gauge> gauges = new ArrayList<>();
		try {
			LocalDateTime horaLastKill = getHoraUltimoKill();
			LocalDateTime horaBusqueda = horaLastKill;
			if (horaBusqueda.isAfter(LocalDateTime.now().minusHours(4))) {
				horaBusqueda = LocalDateTime.now().minusHours(4);
			}
			List<Vela_005> velas = vela_005Repository.findAllAfterDate(horaBusqueda).stream().filter(VelaBitmex.fechaIgualOSuperiorA(horaBusqueda)).sorted(VelaBitmex.ORDEN_ASCENDENTE).collect(Collectors.toList());
			for (TipoGauge tipo : TipoGauge.values()) {
				switch (tipo) {
				case MIN30:
					List<Vela_005> velas30minutos = velas.stream().filter(v -> v.getHoraCierre().isAfter(LocalDateTime.now(ZoneOffset.UTC).minusMinutes(30))).collect(Collectors.toList());
					gauges.add(new Gauge(tipo.getCodigo(), calcularReparticion(velas30minutos)));
					break;

				case HOUR1:
					List<Vela_005> velas1hora = velas.stream().filter(v -> v.getHoraCierre().isAfter(LocalDateTime.now(ZoneOffset.UTC).minusHours(1))).collect(Collectors.toList());
					gauges.add(new Gauge(tipo.getCodigo(), calcularReparticion(velas1hora)));
					break;
				case HOUR2:
					List<Vela_005> velas2hora = velas.stream().filter(v -> v.getHoraCierre().isAfter(LocalDateTime.now(ZoneOffset.UTC).minusHours(2))).collect(Collectors.toList());
					gauges.add(new Gauge(tipo.getCodigo(), calcularReparticion(velas2hora)));
					break;
				case HOUR4:
					List<Vela_005> velas4hora = velas.stream().filter(v -> v.getHoraCierre().isAfter(LocalDateTime.now(ZoneOffset.UTC).minusHours(4))).collect(Collectors.toList());
					gauges.add(new Gauge(tipo.getCodigo(), calcularReparticion(velas4hora)));
					break;
				case LAST_KILL:
					List<Vela_005> velasLastKill = velas.stream().filter(v -> v.getHoraCierre().isAfter(horaLastKill)).collect(Collectors.toList());
					gauges.add(new Gauge(tipo.getCodigo(), calcularReparticion(velasLastKill)));
					break;
				}
			}

		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerDatosGauges" + e);
		}
		return gauges;
	}

	private int calcularReparticion(List<Vela_005> velas) {
		int resultado = 50;
		try {
			BigDecimal sumatorioBuy = BigDecimal.ZERO;
			BigDecimal sumatorioSell = BigDecimal.ZERO;
			for (Vela_005 v : velas) {
				sumatorioBuy = sumatorioBuy.add(v.getDineroAcumuladoBuy() == null ? BigDecimal.ZERO : v.getDineroAcumuladoBuy());
				sumatorioSell = sumatorioSell.add(v.getDineroAcumuladoSell() == null ? BigDecimal.ZERO : v.getDineroAcumuladoSell());
			}
			if (sumatorioSell != null && !BigDecimal.ZERO.equals(sumatorioSell)) {
				resultado = sumatorioBuy.divide(sumatorioBuy.add(sumatorioSell), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).intValue();
			}
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante calcularReparticion" + e);
		}
		return resultado;
	}

	private MainData obtenerDatosMainData(LocalDateTime hora, TemporalidadJSON temporalidadElegida) throws JsonExcepcion {
		MainData mainData = new MainData();
		try {

			List<Vela_005> velas5minutos = vela_005Repository.findAllAfterDate(hora).stream().filter(VelaBitmex.fechaIgualOSuperiorA(hora)).sorted(VelaBitmex.ORDEN_ASCENDENTE).collect(Collectors.toList());
			if (temporalidadElegida == TemporalidadJSON.LAST_KILL && !velas5minutos.isEmpty() && velas5minutos.size() > 1) {
				velas5minutos = velas5minutos.stream().sorted((rt1, rt2) -> rt1.getHoraCierre().compareTo(rt2.getHoraCierre())).collect(Collectors.toList());
				velas5minutos.remove(0);
			}
			LocalDateTime ultimoKill = getHoraUltimoKill();
			BigDecimal buyCount = BigDecimal.ZERO;
			BigDecimal buyPrice = BigDecimal.ZERO;
			BigDecimal buySize = BigDecimal.ZERO;
			BigDecimal volumenAcumuladoBuy = BigDecimal.ZERO;
			BigDecimal sellCount = BigDecimal.ZERO;
			BigDecimal volumenAcumuladoSell = BigDecimal.ZERO;
			BigDecimal sellPrice = BigDecimal.ZERO;
			BigDecimal sellSize = BigDecimal.ZERO;

			BigDecimal precioVolumenAcumuladoBuy = BigDecimal.ZERO;
			BigDecimal precioVolumenAcumuladoSell = BigDecimal.ZERO;

			for (Vela_005 vela : velas5minutos) {
				if (vela.getVolumenSell() != null) {

					volumenAcumuladoSell = volumenAcumuladoSell.add(vela.getVolumenSell());
					precioVolumenAcumuladoSell = precioVolumenAcumuladoSell.add(vela.getDineroAcumuladoSell());
				}

				if (vela.getCantidadSell() != null) {
					sellCount = sellCount.add(new BigDecimal(vela.getCantidadSell()));
				}

				if (vela.getCantidadBuy() != null) {
					buyCount = buyCount.add(new BigDecimal(vela.getCantidadBuy()));

				}
				if (vela.getVolumenBuy() != null) {
					volumenAcumuladoBuy = volumenAcumuladoBuy.add(vela.getVolumenBuy());

					precioVolumenAcumuladoBuy = precioVolumenAcumuladoBuy.add(vela.getDineroAcumuladoBuy());
				}
			}

			buySize = volumenAcumuladoBuy;
			sellSize = volumenAcumuladoSell;

			if (sellPrice != null && !BigDecimal.ZERO.equals(volumenAcumuladoSell) && !BigDecimal.ZERO.equals(precioVolumenAcumuladoSell)) {
				sellPrice = precioVolumenAcumuladoSell.divide(volumenAcumuladoSell, 1, RoundingMode.HALF_UP);
			}

			if (buyPrice != null && !BigDecimal.ZERO.equals(volumenAcumuladoBuy) && !BigDecimal.ZERO.equals(precioVolumenAcumuladoBuy)) {
				buyPrice = precioVolumenAcumuladoBuy.divide(volumenAcumuladoBuy, 1, RoundingMode.HALF_UP);
			}

			Funding funding = bitmexSv.obtenerFunding();

			OffsetDateTime fundingTimpestamp = null;
			Double fundingRate = null;
			Double fundingRateDaily = null;
			if (funding != null) {
				if (funding.getTimestamp() != null) {
					fundingTimpestamp = funding.getTimestamp();
				}
				if (funding.getFundingRate() != null) {
					fundingRate = funding.getFundingRate();
				}
				if (funding.getFundingRateDaily() != null) {
					fundingRateDaily = funding.getFundingRateDaily();
				}
			}
			mainData = new MainData(buyCount, buyPrice, buySize, sellCount, sellPrice, sellSize, ultimoKill, fundingTimpestamp, fundingRate, fundingRateDaily);
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerDatosMainData" + e);
			throw e;
		}
		return mainData;

	}

	private DatosMainGraph obtenerDatosMainGraphPools(LocalDateTime hora) {

		List<MainGraph> listMainGraph = new ArrayList<>();
		List<Vela_005> velas = new ArrayList<>();
		try {

			velas = vela_005Repository.findAllAfterDate(hora);
			velas = velas.stream().filter(VelaBitmex.fechaIgualOSuperiorA(hora)).sorted(VelaBitmex.ORDEN_ASCENDENTE).collect(Collectors.toList());
			for (Vela_005 v : velas) {
				VelaStockPoolDTO velaStockPool = new VelaStockPoolDTO(v.getHoraCierre(), v.getOpen(), v.getClose(), v.getHigh(), v.getLow(), v.getVolumen(), null, null, null, null, null);
				MainGraph mainGraphPool = new MainGraph(velaStockPool);
				listMainGraph.add(mainGraphPool);
			}
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerDatosMainGraph" + e);
		}

		return new DatosMainGraph(listMainGraph);
	}

	public ValoresPools obtenerPoolsMaximoMinimo(LocalDateTime horaInicio, FiltroMillones filtro, Map<Vela_005, List<Object[]>> mapVelasPools) throws JsonExcepcion {
		ValoresPools vp = new ValoresPools();
		try {
			vp = construirPoolsMaximoMinimo(horaInicio, filtro, mapVelasPools);
			construirHeatMap(vp, filtro);
		} catch (ConstruirPoolsExcepcion | HeatmapExcepcion e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante obtenerPools" + e);
		}

		return vp;
	}

	/**
	 * MÃ©todo encargado de construir la imagen del Heatmap
	 * 
	 * 
	 * @param vp
	 * @param filtro
	 * @throws HeatmapExcepcion
	 * 
	 */
	private void construirHeatMap(ValoresPools vp, FiltroMillones filtro) throws HeatmapExcepcion {
//		log.info("====================");

//		log.info("CONSTRUIR HEATMAP {}", filtro.getSufijo());
		try {
			Double numEjesY = (vp.getMaxPrecio().subtract(vp.getMinPrecio())).doubleValue();

			double[][] data = new double[numEjesY.intValue()][vp.getVelas().size()];

			int indexEjeY = 0;

			double valorMaximo = 0d;
			double valorMinimo = 1000000000000000d;
			for (BigDecimal precio = vp.getMaxPrecio(); precio.compareTo(vp.getMinPrecio()) > 0; precio = precio.subtract(new BigDecimal(5))) {

				int indexEjeX = 0;
				for (Vela_005 v : vp.getVelas()) {
					Map<BigDecimal, VelaStockPoolDTO> mapVela = vp.getMapGlobal().get(v);

					VelaStockPoolDTO velaDTO = mapVela.get(precio);
					double valor = 0d;
					if (velaDTO != null) {

						valor = velaDTO.getSumaTotal().doubleValue();
					}

					if (valorMaximo < valor) {
						valorMaximo = valor;
					}

					if (valorMinimo > valor) {
						valorMinimo = valor;
					}

					for (int i = 0; i < 5; i++) {
						data[indexEjeY][indexEjeX] = valor;

						indexEjeY++;
					}
					indexEjeY -= 5;

					indexEjeX++;
				}
				indexEjeY += 5;

			}
			convertirImagenes(vp, filtro, numEjesY, data, valorMinimo, valorMaximo);
		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante la cración del HeatMap" + e);
			throw e;
		}
	}

	private void convertirImagenes(ValoresPools vp, FiltroMillones filtro, Double numEjesY, double[][] data, double valorMinimo, double valorMaximo) {
		// Step 1: Create our heat map chart using our data.

		List<String> lineas = new ArrayList<>();
		for (int y = 0; y < numEjesY.intValue(); y++) {
			String linea = "";

			for (int x = 0; x < vp.getVelas().size(); x++) {
				linea = linea + data[y][x] + ";";
			}

			lineas.add(linea);
		}
		HeatChart map = new HeatChart(data, valorMinimo, valorMaximo);
		map.setShowXAxisValues(false);
		map.setShowYAxisValues(false);
		map.setLowValueColour(Color.black);
		map.setHighValueColour(Color.yellow);
		map.setAxisThickness(0);
		map.setChartMargin(0);

		String rutaFull = rutaHeatmapFull + filtro.getSufijo() + ".png";
		try {
			map.saveToFile(new File(rutaFull));
			File input = new File(rutaFull);
			BufferedImage image = ImageIO.read(input);
			BufferedImage resized = resize(image, 1000, 1000);
			File output = new File(rutaHeatmap + filtro.getSufijo() + ".png");
			ImageIO.write(resized, "png", output);

		} catch (IOException e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante la cración del HeatMap" + e);
		}
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	/**
	 * MÃ©todo encargado de construir los ValoresPools
	 * 
	 * 
	 * @param horaInicio
	 * @param filtro
	 * 
	 * @param mapVelasPools
	 * @param maxPrecio
	 * @param minPrecio
	 * 
	 * @return
	 * @throws ConstruirPoolsExcepcion
	 */

	private ValoresPools construirPools(LocalDateTime horaInicio, FiltroMillones filtro, List<Vela_005> velas, Map<Vela_005, List<Object[]>> mapVelasPools) throws ConstruirPoolsExcepcion {
		ValoresPools vp = new ValoresPools();
		try {

//			List<BigDecimal> preciosPools = new ArrayList<>();
			Map<Vela_005, Map<BigDecimal, VelaStockPoolDTO>> mapGlobal = new HashMap<>();
			Map<String, String> mapTooltips = new HashMap<String, String>();

			List<Object[]> poolsAnteriores = new ArrayList<>();
			boolean usandoPoolsAnteriores = false;
			BigDecimal minPrecio = new BigDecimal(100000000);

			BigDecimal maxPrecio = BigDecimal.ZERO;
			for (Vela_005 v : velas) {
				Map<BigDecimal, VelaStockPoolDTO> mapVela = new HashMap<>();

				List<Object[]> pools = mapVelasPools.get(v);
				if (CollectionUtils.isEmpty(pools)) {
					pools = poolsAnteriores;

					usandoPoolsAnteriores = true;
				}
				for (Object[] object : pools) {
					Timestamp horaTimestamp = (Timestamp) object[0];
					if (usandoPoolsAnteriores) {
						horaTimestamp.setTime(horaTimestamp.getTime() + TimeUnit.MINUTES.toMillis(5));
					}
					Integer precio = (Integer) object[1];
					if (poolValido(v, new BigDecimal(precio))) {

						BigDecimal sumaTotal = (BigDecimal) object[5];
						VelaStockPoolDTO velaStockPoolObject = null;
						if (sumaTotal.compareTo(new BigDecimal(filtro.getValor())) >= 0) {

							velaStockPoolObject = new VelaStockPoolDTO(horaTimestamp.toLocalDateTime(), null, null, null, null, null, precio, (BigDecimal) object[2], (BigDecimal) object[3], (BigDecimal) object[4], sumaTotal);
							mapTooltips.put(velaStockPoolObject.getHoraCierreString("HH:mm") + velaStockPoolObject.getPrecio().setScale(0), velaStockPoolObject.getTooltip());
							mapVela.put(velaStockPoolObject.getPrecio(), velaStockPoolObject);

						} else {
							continue;
						}

					}

					if (minPrecio.compareTo(new BigDecimal(precio)) > 0) {
						minPrecio = new BigDecimal(precio);
					}
					if (maxPrecio.compareTo(new BigDecimal(precio)) < 0) {
						maxPrecio = new BigDecimal(precio);
					}

					if (minPrecio.compareTo(v.getLow()) > 0) {
						minPrecio = v.getLow();
					}
					if (maxPrecio.compareTo(v.getHigh()) < 0) {
						maxPrecio = v.getHigh();
					}
				}
				usandoPoolsAnteriores = false;
				poolsAnteriores = pools;
				mapGlobal.put(v, mapVela);
			}
			vp = new ValoresPools(mapGlobal, mapTooltips, minPrecio, maxPrecio, velas);

		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante la cración de los pools" + e);
		}
		return vp;
	}

	/**
	 * MÃ©todo encargado de construir los ValoresPools
	 * 
	 * 
	 * @param horaInicio
	 * @param filtro
	 * @param mapVelasPools
	 * 
	 * @param maxPrecio
	 * @param minPrecio
	 * @return
	 * 
	 * @throws ConstruirPoolsExcepcion
	 */
	private ValoresPools construirPoolsMaximoMinimo(LocalDateTime horaInicio, FiltroMillones filtro, Map<Vela_005, List<Object[]>> mapVelasPools) throws ConstruirPoolsExcepcion {
		ValoresPools vp = new ValoresPools();
		try {
			Map<Vela_005, Map<BigDecimal, VelaStockPoolDTO>> mapGlobal = new HashMap<>();
			Map<String, String> mapTooltips = new HashMap<String, String>();
			List<Vela_005> velas = mapVelasPools.keySet().stream().sorted(Vela_005.ORDEN_ASCENDENTE).collect(Collectors.toList());
			for (Vela_005 v : velas) {
				Map<BigDecimal, VelaStockPoolDTO> mapVela = new HashMap<>();
				List<Object[]> pools = mapVelasPools.get(v);
				for (Object[] object : pools) {
					Timestamp horaTimestamp = (Timestamp) object[0];
					Integer precio = (Integer) object[1];
					if (poolValido(v, new BigDecimal(precio))) {
						BigDecimal sumaTotal = (BigDecimal) object[5];
						VelaStockPoolDTO velaStockPoolObject = null;
						if (sumaTotal.compareTo(new BigDecimal(filtro.getValor())) >= 0) {
							velaStockPoolObject = new VelaStockPoolDTO(horaTimestamp.toLocalDateTime(), null, null, null, null, null, precio, (BigDecimal) object[2], (BigDecimal) object[3], (BigDecimal) object[4], sumaTotal);
							mapTooltips.put(velaStockPoolObject.getHoraCierreString("HH:mm") + velaStockPoolObject.getPrecio().setScale(0), velaStockPoolObject.getTooltip());
							mapVela.put(velaStockPoolObject.getPrecio(), velaStockPoolObject);

						} else {
							continue;
						}
					}
					if (JsonSvHelper.getMinimo().compareTo(new BigDecimal(precio)) > 0 && filtro == FiltroMillones.DIEZ) {
						JsonSvHelper.setMinimo(new BigDecimal(precio));
					}
					if (JsonSvHelper.getMaximo().compareTo(new BigDecimal(precio)) < 0 && filtro == FiltroMillones.DIEZ) {
						JsonSvHelper.setMaximo(new BigDecimal(precio));
					}

					if (JsonSvHelper.getMinimo().compareTo(v.getLow()) > 0 && filtro == FiltroMillones.DIEZ) {
						JsonSvHelper.setMinimo(v.getLow());
					}

					if (JsonSvHelper.getMaximo().compareTo(v.getHigh()) < 0 && filtro == FiltroMillones.DIEZ) {
						JsonSvHelper.setMaximo(v.getHigh());
					}
				}
				mapGlobal.put(v, mapVela);
			}
			vp = new ValoresPools(mapGlobal, mapTooltips, JsonSvHelper.getMinimo(), JsonSvHelper.getMaximo(), velas);

		} catch (Exception e) {
			log.error("StackTrace del error: ", e);
			datosEnMemoria.getTelegramSv().notifyMessage("Excepción durante la cración de los pools" + e);
			throw e;
		}

		return vp;

	}

	/**
	 * 
	 * Metodo que valida que el pool no haya tocado la vela
	 * 
	 * @param v
	 * 
	 * @param precioPool
	 * @return
	 */

	private boolean poolValido(Vela_005 v, BigDecimal precioPool) {
		return !(precioPool.compareTo(v.getLow()) > 0 && precioPool.compareTo(v.getHigh()) < 0);
	}

	@NoArgsConstructor
	@Getter

	public class ValoresPools {
		BigDecimal minPrecio = new BigDecimal(100000000);
		BigDecimal maxPrecio = BigDecimal.ZERO;
		Map<Vela_005, Map<BigDecimal, VelaStockPoolDTO>> mapGlobal = new HashMap<>();
		@Getter
		Map<String, String> mapTooltips = new HashMap<>();
		List<Vela_005> velas = new ArrayList<>();

		public ValoresPools(Map<Vela_005, Map<BigDecimal, VelaStockPoolDTO>> mapGlobal, Map<String, String> mapTooltips, BigDecimal minPrecio, BigDecimal maxPrecio, List<Vela_005> velas) {
			this.mapGlobal = mapGlobal;
			this.mapTooltips = mapTooltips;
			this.minPrecio = minPrecio;
			this.maxPrecio = maxPrecio;
			this.velas = velas;
		}

		public Vela_005 getVelaInicial() {
			Optional<Vela_005> v = velas.stream().sorted((v1, v2) -> v1.getHoraCierre().compareTo(v2.getHoraCierre())).findFirst();
			if (v.isPresent()) {
				return v.get();
			} else {
				Vela_005 vela = new Vela_005();
				vela.setHoraCierre(LocalDateTime.now().minusYears(10));
				return new Vela_005();
			}
		}

		public Vela_005 getVelaFinal() {
			Optional<Vela_005> v = velas.stream().sorted((v1, v2) -> v2.getHoraCierre().compareTo(v1.getHoraCierre())).findFirst();
			if (v.isPresent()) {
				return v.get();
			} else {
				Vela_005 vela = new Vela_005();
				vela.setHoraCierre(LocalDateTime.now().minusYears(10));

				return new Vela_005();
			}
		}
	}

	/**
	 * 
	 * Crear el objeto de pools para generar el fichero json
	 * 
	 * @param horaInicio
	 * 
	 * 
	 * @return
	 */

	public String obtenerJSONPools(LocalDateTime horaInicio) {
		DatosScalpingDTO datosScalpingDTO = new DatosScalpingDTO();
		DatosMainGraph datosMainGraph = obtenerDatosMainGraphPools(horaInicio);
		datosScalpingDTO.setMaingraph(datosMainGraph.getMaingraph());
		Gson gson = new Gson();
		return gson.toJson(datosScalpingDTO);
	}

	@Getter
	public class DatosMainGraph {
		public DatosMainGraph(List<MainGraph> listMainGraphOficial) {
			this.maingraph = listMainGraphOficial;
		}

		List<MainGraph> maingraph;
	}
}