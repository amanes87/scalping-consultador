package com.scalpingjedi.consultador.servicio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.telegram.service.TelegramSv;

import lombok.Getter;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class DatosEnMemoria {
	
	private TelegramSv telegramSv;
	
	@Getter
	private boolean servidor;
	
	@Value("${spring.profiles.active}")
	private String entorno;

	public void inicializarTelegram(String chatId, String url) {
		telegramSv = new TelegramSv(chatId, url);
		servidor = "servidor".equals(entorno);
	}

	public void notificarMensaje(String mensaje) {
		if (servidor) {
			telegramSv.notifyMessage(mensaje);
		}
	}
	
	public TelegramSv getTelegramSv() {
		return servidor ? this.telegramSv : null;
	}

	
}
