package com.scalpingjedi.consultador;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.scalpingjedi.comun.entidades.VelaBitmex;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;
import com.scalpingjedi.comun.servicios.Vela_005Repository;
import com.scalpingjedi.comun.servicios.pools.PoolRepository;
import com.scalpingjedi.consultador.controladores.JsonSvHelper;
import com.scalpingjedi.consultador.dtos.PoolsInfoDTO;
import com.scalpingjedi.consultador.dtos.pools.tabla.MapTablaPoolsDTO;
import com.scalpingjedi.consultador.dtos.pools.tabla.PoolsDTO;
import com.scalpingjedi.consultador.dtos.pools.tabla.TablaPoolsDTO;
import com.scalpingjedi.consultador.excepcion.ConstruirArchivosExcepcion;
import com.scalpingjedi.consultador.excepcion.LecturaDirectorioJSONExcepcion;
import com.scalpingjedi.consultador.excepcion.SFTPExcepcion;
import com.scalpingjedi.consultador.excepcion.SshConnectorExcepcion;
import com.scalpingjedi.consultador.servicio.DatosEnMemoria;
import com.scalpingjedi.consultador.servicio.JsonSv;
import com.scalpingjedi.consultador.servicio.JsonSv.ValoresPools;
import com.scalpingjedi.sftp.SFTPConnector;

@PropertySource("classpath:aplicacion.properties")
@Component
public class HiloSFTP {
	
	@Autowired
	private JsonSv jsonSv;
	
	@Autowired
	private DatosEnMemoria datosEnMemoria;
	
	@Autowired
	private Vela_005Repository vela_005Repository;
	
	@Autowired
	private PoolRepository poolRepository;
	
	@Value("${ssh.directory.local}")
	private String directorioTemporalLocal;
	
	@Value("${ssh.directory.remote}")
	private String directoriosRemotos;
	
	@Value("${ssh.connection.username}")
	private String USERNAME;
	
	@Value("${ssh.connection.host}")
	private String HOST;
	
	@Value("${ssh.connection.port}")
	private String PORT;
	
	@Value("${ssh.connection.password}")
	private String PASSWORD;
	
	@Value("${hilo.segundos}")
	private Long numeroSegundos;

	@Value("${horas.tiempo.atras}")
	private Long HORAS_PASADAS;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	
	Map<Vela_005, List<Object[]>> mapVelasPools = new HashMap<>();

	public enum TIPO_ARCHIVOS{
		TIPO_GENERAL, TIPO_POOLS;
	}
	
	@Transactional
	void ejecutarDatosGenerales(String... args) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				construirArchivosGenerales();
				recorrerPropiedadesSSH(TIPO_ARCHIVOS.TIPO_GENERAL);
				log.info("<><><>Fin ARCHIVOS GENERALES<><><>");
			}
			
		}, 0, 15, TimeUnit.SECONDS);
	}
	
	@Transactional
	void ejecutarPools(String... args) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				construirArchivosPools();
				recorrerPropiedadesSSH(TIPO_ARCHIVOS.TIPO_POOLS);
				log.info("####Fin ARCHIVOS POOLS####");
			}
			
		}, 0, 60, TimeUnit.SECONDS);
	}

	/**
	 * Método encargado de escribir los ficheros JSON en disco
	 */
	protected void construirArchivosGenerales() {
		
		LocalDateTime horaInicio = LocalDateTime.now(ZoneOffset.UTC).minusHours(HORAS_PASADAS);
		
		escribirJSONTemporalidadesGeneral(horaInicio);
		
	}
	
	protected void construirArchivosPools() {
		
		LocalDateTime horaInicio = LocalDateTime.now(ZoneOffset.UTC).minusHours(HORAS_PASADAS);
		
		escribirVelas(horaInicio);
		
		escribirHeatMaps(horaInicio);
	}
	
	/**
	 * Método encargado de escribir los JSON de diferentes temporalidades
	 * @param horaInicio
	 */
	private void escribirJSONTemporalidadesGeneral(LocalDateTime horaInicio) {
		Stream.of(TemporalidadJSON.values()).forEach(t ->{
			String json = jsonSv.obtenerJSONTemporalidad(t.getCodigo(), horaInicio);
			try {
				FileUtils.writeByteArrayToFile(new File(directorioTemporalLocal + t.getCodigo() + ".json"), json.getBytes(), false);
			} catch (Exception e) {
				new ConstruirArchivosExcepcion(e, MessageFormat.format("Excepción durante la creación del JSON {0} en el directorio {1}", t.getCodigo(), directorioTemporalLocal), datosEnMemoria.getTelegramSv());
			}
		});
	}

	/**
	 * Metodo encargado de escribir el JSON de velas del gráfico principal
	 * @param horaInicio
	 */
	private void escribirVelas(LocalDateTime horaInicio) {
		String json = jsonSv.obtenerJSONPools(horaInicio);
		try {
			FileUtils.writeByteArrayToFile(new File(directorioTemporalLocal + "pools.json"), json.getBytes(), false);
		} catch (Exception e) {
			new ConstruirArchivosExcepcion(e, MessageFormat.format("Excepción durante la creación del JSON {0} en el directorio {1}", "pools", directorioTemporalLocal), datosEnMemoria.getTelegramSv());
		}
	}

	/**
	 * Metodo encargado de escribir la IMAGEN de los POOLS y generar el PoolsInfo
	 * @param horaInicio
	 */
	private void escribirHeatMaps(LocalDateTime horaInicio) {
		Set<LocalDateTime> horasVelasActual = construirMapVelas(horaInicio);

		limpiarMapVelasPasadas(horasVelasActual);
		
		JsonSvHelper.inicalizarMinimoMaximo();
		MapTablaPoolsDTO mapTablaPoolsDTO = new MapTablaPoolsDTO();
		for (FiltroMillones filtro : FiltroMillones.values()) {
			try {
				ValoresPools valoresPools = jsonSv.obtenerPoolsMaximoMinimo(horaInicio, filtro, mapVelasPools);
				TablaPoolsDTO tablaPool = construirTablasPools(valoresPools, filtro);
				mapTablaPoolsDTO.v.add(tablaPool);
				escribirPoolsInfo(valoresPools, filtro);
			} catch (Exception e) {
				new ConstruirArchivosExcepcion(e, MessageFormat.format("Excepción durante la creación del JSON {0} en el directorio {1}", "poolsInfo", directorioTemporalLocal), datosEnMemoria.getTelegramSv());
			}
		}
		try {
			escribirTablaPools(mapTablaPoolsDTO);
		} catch (IOException e) {
			new ConstruirArchivosExcepcion(e, MessageFormat.format("Excepción durante la creación del JSON tablaPools en el directorio {0}", directorioTemporalLocal), datosEnMemoria.getTelegramSv());
		}
	}

	private void escribirTablaPools(MapTablaPoolsDTO mapTablaPoolsDTO) throws IOException {
		Gson gson = new Gson();
		String tablaPoolsJSON = gson.toJson(mapTablaPoolsDTO);
		FileUtils.writeByteArrayToFile(new File(directorioTemporalLocal + "tablaPools.json"), tablaPoolsJSON.getBytes(), false);
	}

	private void escribirPoolsInfo(ValoresPools valoresPools, FiltroMillones filtro) throws IOException {
		Gson gson = new Gson();
		String jsonPoolsInfo = gson.toJson(new PoolsInfoDTO(valoresPools.getMinPrecio(), valoresPools.getMaxPrecio(), valoresPools.getMapTooltips()));
		FileUtils.writeByteArrayToFile(new File(directorioTemporalLocal + "poolsInfo"+filtro.getSufijo() + ".json"), jsonPoolsInfo.getBytes(), false);
	}

	private TablaPoolsDTO construirTablasPools(ValoresPools valoresPools, FiltroMillones filtro) {
		TablaPoolsDTO tablaPools = new TablaPoolsDTO();
		tablaPools.pool = filtro.getSufijo();
		List<PoolsDTO> pools = new ArrayList<>();
		valoresPools.getMapGlobal().get(valoresPools.getVelaFinal()).values().forEach(valorPool -> {
			pools.add(new PoolsDTO(valorPool));
		});
		tablaPools.v = pools;
		return tablaPools;
	}

	private Set<LocalDateTime> construirMapVelas(LocalDateTime horaInicio) {
		Set<LocalDateTime> horasVelasActual = new HashSet<LocalDateTime>();
		List<Vela_005> velas = vela_005Repository.findAllAfterDate(horaInicio);
		velas = velas.stream().filter(VelaBitmex.fechaIgualOSuperiorA(horaInicio)).sorted(VelaBitmex.ORDEN_ASCENDENTE).collect(Collectors.toList());
		Set<LocalDateTime> horasMapVelas = mapVelasPools.keySet().parallelStream().map(v -> v.getHoraCierre()).collect(Collectors.toSet());
		for (Vela_005 v : velas) {
			if (!horasMapVelas.contains(v.getHoraCierre())) {
				rellenarMapPools(mapVelasPools, v);
			}
			recuperarPoolsAnterioresVacios(mapVelasPools, v);
			horasVelasActual.add(v.getHoraCierre());
		}
		return horasVelasActual;
	}

	/*
	 * Recupera los GAPS en el gráfico de velas y pools
	 */
	private void recuperarPoolsAnterioresVacios(Map<Vela_005, List<Object[]>> mapVelasPools, Vela_005 v) {
		List<Object[]> poolsVela = mapVelasPools.get(v);
		if (CollectionUtils.isEmpty(poolsVela)) {
			rellenarMapPools(mapVelasPools, v);
		}
	}
	
	private void rellenarMapPools(Map<Vela_005, List<Object[]>> mapVelasPools, Vela_005 v) {
		List<Object[]> pools = poolRepository.obtenerPoolsAgrupadosPorHora(v.getHoraCierre());
		mapVelasPools.put(v, pools);
	}

	/**
	 * Metodo que quita las velas pasadas que ya no se mostrarán en el gráfico de pools
	 * @param horasVelasActual
	 */
	private void limpiarMapVelasPasadas(Set<LocalDateTime> horasVelasActual) {
		List<Vela_005> velasPasadas = mapVelasPools.keySet().stream().filter(v -> !horasVelasActual.contains(v.getHoraCierre())).collect(Collectors.toList());
		velasPasadas.forEach(v -> {
			mapVelasPools.remove(v);
		});
	}

	/**
	 * Método encargado de mandar los ficheros JSON del disco al servidor del frontEnd
	 * @param tipoGeneral 
	 */
	protected void recorrerPropiedadesSSH(TIPO_ARCHIVOS tipoArchivos) {
		String[] usuarios = USERNAME.split(",");
		String[] passwords = PASSWORD.split(",");
		String[] hosts = HOST.split(",");
		String[] ports = PORT.split(",");
		for (int i = 0; i < usuarios.length; i++) {
			enviarArchivoViaSFTP(usuarios[i], passwords[i], hosts[i], Integer.valueOf(ports[i]), tipoArchivos);
		}
		
	}

	private void enviarArchivoViaSFTP(String usuario, String password, String host, int port, TIPO_ARCHIVOS tipoArchivos) {
		SFTPConnector sshConnector = new SFTPConnector();
		try {
			sshConnector.connect(usuario, password, host, port);
			try {
				Files.list(Paths.get(directorioTemporalLocal))
				.forEach(f ->{
					boolean enviar = false;
					if (tipoArchivos.equals(TIPO_ARCHIVOS.TIPO_GENERAL) && Stream.of(TemporalidadJSON.values()).map(t -> t.getCodigo()).anyMatch(t -> StringUtils.contains(f.getFileName().toString(), t))) {
						enviar = true;
					} else if (tipoArchivos.equals(TIPO_ARCHIVOS.TIPO_POOLS) && (Stream.of(FiltroMillones.values()).map(t -> t.getSufijo()).anyMatch(t -> StringUtils.contains(f.getFileName().toString(), t)) || StringUtils.contains(f.getFileName().toString(), "pools.json") || StringUtils.contains(f.getFileName().toString(), "tablaPools.json"))) {
						enviar = true;
					}
					if (enviar) {
						String[] directorios = directoriosRemotos.split(",");
						for (int i = 0; i<directorios.length; i++) {
							enviarArchivo(sshConnector, f, directorios[i]);
						}
					}
				});
			} catch (IOException e) {
				new LecturaDirectorioJSONExcepcion(e, MessageFormat.format("Excepción durante la lectura del directorio {0}", directorioTemporalLocal), datosEnMemoria.getTelegramSv());
			}
		} catch (IllegalAccessException | JSchException  e1) {
			new SshConnectorExcepcion(e1, MessageFormat.format("Excepción durante la conexión por SSH: {0},{1},{2},{3}", usuario, password, host, port), datosEnMemoria.getTelegramSv()) ;
		} finally {
			sshConnector.disconnect();
		}
	}

	private void enviarArchivo(SFTPConnector sshConnector, Path f, String directorio) {
		try {
			sshConnector.addFile(directorio, f.toString(), f.getFileName().toString());
		} catch (IllegalAccessException | IOException | SftpException | JSchException e) {
			new SFTPExcepcion(e, MessageFormat.format("Error durante el envío del archivo {0} al directorio remoto {1}", f.toString(), directorio), datosEnMemoria.getTelegramSv());
		}
	}
}
