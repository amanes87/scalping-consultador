package com.scalpingjedi.consultador;


import java.util.stream.Stream;

import lombok.Getter;

public enum TemporalidadJSON {

	MINUTOS_30("30min"), HORAS_1("1hour"), HORAS_2("2hour"), HORAS_4("4hour"), LAST_KILL("lastkill");
	
	@Getter
	private String codigo;
	
	private TemporalidadJSON(String codigo) {
		this.codigo = codigo;
	}
	
	public static TemporalidadJSON getTemporalidad(String temporalidad) {
		TemporalidadJSON temporalidadJSON = Stream.of(TemporalidadJSON.values())//
				.filter(t -> t.getCodigo().equals(temporalidad))//
				.findFirst()//
				.orElse(LAST_KILL);
		return temporalidadJSON;
	}
}
