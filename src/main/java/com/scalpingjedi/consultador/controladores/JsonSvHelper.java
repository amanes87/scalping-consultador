package com.scalpingjedi.consultador.controladores;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Scope(value = "singleton")
public class JsonSvHelper {

	@Getter
	@Setter
	private static BigDecimal minimo;

	@Getter
	@Setter
	private static BigDecimal maximo;

	public static void inicalizarMinimoMaximo() {
		minimo = new BigDecimal(100000000);
		maximo = BigDecimal.ZERO;
	}

}
