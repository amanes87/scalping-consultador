package com.scalpingjedi.consultador.controladores;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scalpingjedi.comun.servicios.Vela_005Repository;
import com.scalpingjedi.comun.servicios.pools.PoolRepository;
import com.scalpingjedi.consultador.FiltroMillones;
import com.scalpingjedi.consultador.excepcion.JsonExcepcion;
import com.scalpingjedi.consultador.servicio.JsonSv;

@Scope(value = "singleton")
@RestController
public class VelasController {

	@Autowired
	private Vela_005Repository vela_005Repository;

	@Autowired
	private PoolRepository poolRepository;

	@Autowired
	private JsonSv jsonSv;
	
	private final Integer HORAS_PASADAS = 15;
	
	@CrossOrigin(origins = "http://localhost")
	@RequestMapping(value = "/data", method = RequestMethod.GET)
	public ResponseEntity<String> getVelas(HttpServletRequest request, HttpServletResponse response,//
			@RequestParam(value="timeframe", required=true) String timeFrame
			) {
		LocalDateTime horaInicio = LocalDateTime.now(ZoneOffset.UTC).minusHours(HORAS_PASADAS);
		return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS)).body(jsonSv.obtenerJSONTemporalidad(timeFrame, horaInicio));
	}
	
	@CrossOrigin(origins = "http://localhost")
	@RequestMapping(value = "/data/pools/info", method = RequestMethod.GET)
	public ResponseEntity<String> getPools(HttpServletRequest request, HttpServletResponse response) throws IOException, JsonExcepcion {
		LocalDateTime horaInicio = LocalDateTime.now(ZoneOffset.UTC).minusHours(HORAS_PASADAS);
		//TODO AQUI RECIBIR POR PATHPARAM EL PARAMETRO FILTRO, VELAS, MAP VELAS Y POOLS
//		return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS)).body(jsonSv.obtenerPoolsMaximoMinimo(horaInicio, FiltroMillones.VEINTE, new HashMap<>()));
		return null;
	}
	
	@CrossOrigin(origins = "http://localhost")
	@RequestMapping(value = "/data/pools", method = RequestMethod.GET)
	public ResponseEntity<String> getVelas(HttpServletRequest request, HttpServletResponse response ) {
		LocalDateTime horaInicio = LocalDateTime.now(ZoneOffset.UTC).minusHours(HORAS_PASADAS);
		return ResponseEntity.ok().cacheControl(CacheControl.maxAge(1, TimeUnit.DAYS)).body(jsonSv.obtenerJSONPools(horaInicio));
	}
}
