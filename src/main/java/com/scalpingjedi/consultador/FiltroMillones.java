package com.scalpingjedi.consultador;

import lombok.Getter;

public enum FiltroMillones {

	DIEZ(10000000, "10"),//
	VEINTE(20000000, "20"),//
	TREINTA(30000000, "30"),//
	CINCUENTA(50000000, "50"),//
	SETEINTAYCINCO(70000000, "70"),//
	CIEN(100000000, "100");//
	
	@Getter
	private Integer valor;
	
	@Getter
	private String sufijo;
	
	private FiltroMillones(Integer valor, String sufijo) {
		this.valor = valor;
		this.sufijo = sufijo;
	}
	
	
}
