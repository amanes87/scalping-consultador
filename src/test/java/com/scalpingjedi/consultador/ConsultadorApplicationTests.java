package com.scalpingjedi.consultador;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.scalpingjedi.sftp.SFTPConnector;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsultadorApplicationTests {

	@Test
	public void contextLoads() {
	}

//	@Test
	public void escribirFicheroRemoto() {
		String USERNAME = "root";
		String HOST = "cloud.settingconsultoria.com";
		int PORT = 22;
		String PASSWORD = "Q1w2e3r4T5";

		try {
			
			File f = new File("C:/temp/pruebaEscritura.txt");
			FileUtils.writeByteArrayToFile(f, "PRUEBA NUEVA".getBytes());
			
			SFTPConnector sshConnector = new SFTPConnector();
			sshConnector.connect(USERNAME, PASSWORD, HOST, PORT);
			sshConnector.addFile("/prueba", "C:/temp/pruebaEscritura.txt", "pruebaSFTP.txt");
			sshConnector.disconnect();
		} catch (JSchException ex) {
			ex.printStackTrace();

			System.out.println(ex.getMessage());
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();

			System.out.println(ex.getMessage());
		} catch (IOException ex) {
			ex.printStackTrace();

			System.out.println(ex.getMessage());
		} catch (SftpException ex) {
			ex.printStackTrace();

			System.out.println(ex.getMessage());
		}
	}

}
